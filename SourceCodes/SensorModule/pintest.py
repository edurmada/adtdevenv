import RPi.GPIO as GPIO
import time

print "finished iports"
GPIO.setmode(GPIO.BCM)
GPIO.setup(4,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

print "setup done!"

print "Readout process started..."
while True:
    print(GPIO.input(4))
    time.sleep(1)
