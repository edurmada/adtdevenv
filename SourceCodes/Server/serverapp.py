import webapp2
import Server

class MainPage(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        reply = Server.get_hello_world()
        self.response.write(reply)

app = webapp2.WSGIApplication([
    ('/', MainPage),
], debug = True)
