#!/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler
from os import curdir, sep, path
from urlparse import urlparse, parse_qsl, urlsplit

SENSOR_HANDLER = None
CONTROL_HANDLER = None

EXTENSION_MANAGEMENT = {
    ".html": 'text/html',
    ".css": 'text/css',
    ".woff": 'text/woff',
    ".woff2": 'text/woff2',
    ".ttf": 'text/ttf',
    ".svg": 'text/svg',
    ".eot": 'text/eot',
    ".jpg" : 'image/jpg',
    ".png" : 'image/png',
    ".gif" : 'image/gif',
    ".ico" : 'image/ico',
    ".js"  : 'application/javascript',
    ".py"  : None,
}

# This class will handles any incoming request from
# the browser
class myHandler(BaseHTTPRequestHandler):

    # Handler for the GET requests
    def do_GET(self):
        if self.path == "/":
            self.path = "/Home.html"

        # Variable & Querie management
        query = dict(parse_qsl(urlsplit(self.path).query))
        self.path = urlparse(self.path).path

        try:
            # Check the file extension required and
            # set the right mime type

            sendReply = False
            for ext in EXTENSION_MANAGEMENT:
                if self.path.endswith(".py"):
                    script = path.join('scripts', self.path[1:])
                    if not path.exists(script):
                        self.send_error(404, 'File Not Found: %s' % script)
                        return
                    if "method" not in query:
                        self.send_error(404, 'Method missing in url: %s' % script)
                        return
                    script_vars = {}
                    execfile(script, script_vars)
                    try:
                        return_value, content_type = script_vars[query['method']](SENSOR_HANDLER, CONTROL_HANDLER, **query)
                    except ValueError as e:
                        self.wfile.write(str(e))
                        return
                    self.send_response(200)
                    self.send_header('Content-type', content_type)
                    self.end_headers()
                    self.wfile.write(return_value)
                    return

                elif self.path.endswith(ext):
                    mimetype = EXTENSION_MANAGEMENT[ext]
                    sendReply = True

            if sendReply == True:
                # Open the static file requested and send it
                f = open(curdir + sep + self.path, 'rb')
                self.send_response(200)
                self.send_header('Content-type', mimetype)
                self.end_headers()
                page = f.read()
                # Updatepage values if needed
                self.wfile.write(page)
                f.close()
            return

        except IOError:
            self.send_error(404, 'File Not Found: %s' % self.path)

    # Handler for the POST requests
    def do_POST(self):
        query = dict(parse_qsl(urlsplit(self.path).query))
        self.path = urlparse(self.path).path
        if self.path.endswith(".py"):
            script = path.join('scripts', self.path[1:])
            if not path.exists(script):
                self.send_error(404, 'File Not Found: %s' % script)
                return
            if "method" not in query:
                self.send_error(404, 'Method missing in url: %s' % script)
                return
            script_vars = {}
            execfile(script, script_vars)
            try:
                script_vars[query['method']](SENSOR_HANDLER, CONTROL_HANDLER, **query)
            except ValueError as e:
                self.wfile.write(str(e))
                return
            self.send_response(200)
            return

    #----------------------------------------------------------- Related methods

    def get_path_extension(self):
        "Returns extension of path"
        return self.path[self.path.rfind("."):]

    def log_message(self, *args, **params):
        pass
#         sys.stderr.write("%s - - [%s] %s\n" %
#                          (self.client_address[0],
#                           self.log_date_time_string(),
#                           format%args))

def get_hello_world():
    return "HELLO WORLD!"


