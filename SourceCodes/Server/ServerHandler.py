
import time
import Server
from BaseHTTPServer import HTTPServer
import os
import serial
import scripts

#===============================================================================
#-------------------------------------------------------------- # SERVER HANDLER
#===============================================================================

PORT_NUMBER = 8080
SERVER = SERVER_THREAD = None
EVENT_LOGGER = scripts.EventLogger()
SENSOR_HANDLER = scripts.SensorHandler(logger = EVENT_LOGGER)
Server.SENSOR_HANDLER = SENSOR_HANDLER

CONTROL_HANDLER = scripts.ControlHandler(logger = EVENT_LOGGER)
Server.CONTROL_HANDLER = CONTROL_HANDLER


PORT = 'COM3'
BAUDRATE = 115200
SERIAL_CONNECTION = scripts.SerialPipe(port = PORT, baudrate = BAUDRATE)
SERIAL_CONNECTION.open()
SERIAL_CONNECTION._start_pipe(SENSOR_HANDLER, CONTROL_HANDLER)

#--------------------------------------------------------------- SENSOR COMMANDS

def set_sensors_off():
    print '>>> Setting sensors off: ',
    for s in SENSOR_HANDLER.sensors:
        s.set_sensor_off()
        print ".",
    time.sleep(1)
    print " DONE!"

def set_sensors_on():
    print SENSOR_HANDLER.sensors
    print '>>> Setting sensors on: ',
    for s in SENSOR_HANDLER.sensors:
        s.set_sensor_on()
        print ".",
    time.sleep(1)
    print " DONE!"

#--------------------------------------------------------------- SERVER COMMANDS

def start_server():
    global SERVER
    global SERVER_THREAD
    if SERVER is not None:
        print ">>> Server already running!"
        return
    from threading import Thread
    # Create a web server and define the handler to manage the
    # incoming request
    print '>>> Creating server...'
    SERVER = HTTPServer(('', PORT_NUMBER), Server.myHandler)
    if not SERVER:
        return 1
    print '>>> Started server on port ' , PORT_NUMBER
    # Wait forever for incoming http requests
    SERVER_THREAD = Thread(target = SERVER.serve_forever)
    if not SERVER_THREAD:
        return 2
    SERVER_THREAD.start()
    return 0


def shutdown_server():
    global SERVER
    global SERVER_THREAD
    print ">>> WAITING FOR SERIAL PORT TO CLOSE...",
    SERIAL_CONNECTION.close()
    print "DONE!"
    if SERVER == None:
        print ">>> Server is closed!"
        return
    SERVER.shutdown()
    # Merge threads
    print '>>> Waiting for thread merge: ',
    while SERVER_THREAD.isAlive():
        SERVER_THREAD.join(timeout = 1)
        print ".",
    print " DONE!"
    set_sensors_off()
    SENSOR_HANDLER.save_env()
    SERVER.socket.close()
    SERVER = SERVER_THREAD = None
    print ">>> Server correctly closed!"

def reload_modules():
    print ">>> Reloading modules... ",
    reload(Server)
    reload(scripts)
    Server.SENSOR_HANDLER = SENSOR_HANDLER
    print "DONE!"

def restart_server():
    shutdown_server()
    reload_modules()
    start_server()

def clear_screen():
    try:
        os.system("cls")
    except:
        os.system("clear")

def help_info():
    print """

These are the accepted commands:
    'start'            : used to start server.
    'exit'             : used to power off server and close the console
    'reset'            : used to return the server to ints original state
    'restart'          : used to restart the server
    'setsensors_off'   : set all sensors off
    'help'             : display this message
    """

ACCEPTED_CMDS = {
    'start'   : start_server,
    'cls'     : clear_screen,
    'poweroff': shutdown_server,
    'exit'    : shutdown_server,
    'restart' : restart_server,
    'sensoroff' : set_sensors_off,
    'sensoron' : set_sensors_on,
    'help'    : help_info,
}

if __name__ == "__main__":

    # waiting for command input
    print '>>>waiting for command input...'
    cmd = ""
    while cmd != 'exit':
        cmd = raw_input("\nCMD: ")
        if cmd == "":
            continue
        if cmd not in ACCEPTED_CMDS.keys():
            print 'ERROR: no command "%s". Type "help" to see available options.' % cmd
        else:
            ACCEPTED_CMDS[cmd]()
        if cmd == 'exit':
            break
    print '>>> "exit" command received, exiting shell... DONE!'

#===============================================================================
#------------------------------------------------------------------------- # APP
#===============================================================================
#
# import webapp2
#
# app = webapp2.WSGIApplication([
#     ('/', MainPage),
# ], debug = True)
