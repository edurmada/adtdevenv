'''
Created on 19/4/2016

@author: Emadariaga

This module gathers information from sensros and uses template library to change it to correct format
'''
import scripts
import random
from datetime import datetime

#===============================================================================
#----------------------------------------------------- # GENERAL PURPOSE LIBRARY
#===============================================================================

#-------------------------------------------------------------------- SENSOR LIB

def get_sensor_data(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    j = scripts.donut_chart_template(SENSOR_HANDLER.get_sensor_usage())
    return j, 'application/json'

def get_discriminated_sensor_data(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    data = SENSOR_HANDLER.get_discriminated_sensor_usage()
    xml = scripts.return_discriminated_sensor_usage(data = data)

    return xml, 'text/html'

def get_sensor_socket_usage(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    data = SENSOR_HANDLER.get_sensor_socket_usage()
    xml = scripts.return_sensor_socket_usage(data)
    return xml, 'text/html'

def get_graph_data(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    """ returns a line graph given the measurement values
    """
    if len(SENSOR_HANDLER.sensors) == 0:
        return "ERROR: no sensors set yet", 'text/html'
    for s in SENSOR_HANDLER.sockets:
        socket_id = '%s %d' % (s.type.upper(), s.number)
        if socket_id == params['socket_id']:
            sensor_id = s.sensor_id
            break
    for sensor in SENSOR_HANDLER.sensors:
        if sensor.id == sensor_id:
            data = sensor.get_history(samples = int(params['samples']))
            break
    j = scripts.line_graph_template(data = {'Sensor Readings':data['values']}, x_max = [data['xmin'], data['xmax']], y_max = [data['ymin'], data['ymax']])
    return j, 'application/json'

def get_sensor_button_group(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    """ returns a line graph given the measurement values
    """
    data = {'sockets':SENSOR_HANDLER.sockets, 'sensors': SENSOR_HANDLER.sensors}
    xml = scripts.return_sensors_button_group(data = data)
    return xml, 'text/html'


def get_all_activity_information(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    header_row = ['Date & Time', 'Object', 'Description']
    if SENSOR_HANDLER.logger.is_empty():
        table_data = [
            ['N/A', 'N/A', 'N/A'],
        ]
    else:
        table_data = SENSOR_HANDLER.logger.get_events_as_table(number = -1)
    xml = scripts.get_custom_table(table_data, header_row)
    return xml, 'text/html'

def get_last_activity_information(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    header_row = ['Date & Time', 'Object', 'Description']
    if SENSOR_HANDLER.logger.is_empty():
        table_data = [
            ['N/A', 'N/A', 'N/A'],
        ]
    else:
        table_data = SENSOR_HANDLER.logger.get_events_as_table(number = 8)
    xml = scripts.get_custom_table(table_data, header_row)
    return xml, 'text/html'

def get_active_sockets(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    d = SENSOR_HANDLER.return_active_sockets()
    j = scripts.dict_to_json(d)
    return j, 'application/json'

def set_sensor(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    SENSOR_HANDLER.add_sensor(**params)
    return 0

def get_sensor_info_table_by_socket(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    header_row = ['Atribute', 'Current value']
    table_data = SENSOR_HANDLER.sensor_info_by_socket(**params)
    xml = scripts.get_custom_table(table_data, header_row)
    return xml, 'text/html'

def modify_sensor(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    SENSOR_HANDLER.modify_sensor(**params)
    return 0

def delete_sensor(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    SENSOR_HANDLER.delete_sensor(**params)
    return 0


#------------------------------------------------------------------- CONTROL LIB

def get_control_socket_usage(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    data = CONTROL_HANDLER.get_control_socket_usage()
    xml = scripts.return_control_socket_usage(data)
    return xml, 'text/html'

def get_discriminated_control_data(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    data = CONTROL_HANDLER.get_discriminated_control_usage()
    xml = scripts.return_discriminated_control_usage(data = data)

    return xml, 'text/html'

def get_control_data(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    j = scripts.donut_chart_template(CONTROL_HANDLER.get_control_current_status(), sensors = False)
    return j, 'application/json'


def get_control_graph_data(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    """ returns a line graph given the measurement values
    """
    if len(CONTROL_HANDLER.controls) == 0:
        return "ERROR: no controls set yet", 'text/html'
    for s in CONTROL_HANDLER.sockets:
        socket_id = '%s %d' % (s.type.upper(), s.number)
        if socket_id == params['socket_id']:
            control_id = s.control_id
            break
    for control in CONTROL_HANDLER.controls:
        if control.id == control_id:
            data = control.get_history(samples = int(params['samples']))
            break
    j = scripts.line_graph_template(data = {'control Readings':data['values']}, x_max = [data['xmin'], data['xmax']], y_max = [data['ymin'], data['ymax']])
    return j, 'application/json'

def get_control_button_group(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    """ returns a line graph given the measurement values
    """
    data = {'sockets':CONTROL_HANDLER.sockets, 'controls': CONTROL_HANDLER.controls}
    xml = scripts.return_controls_button_group(data = data)
    return xml, 'text/html'


def get_control_group_and_status(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    """ returns a line graph given the measurement values
    """
    data = {'sockets':CONTROL_HANDLER.sockets, 'controls': CONTROL_HANDLER.controls}
    xml = scripts.return_controls_group_and_status(data = data)
    return xml, 'text/html'

# def get_all_activity_information(SENSOR_HANDLER, CONTROL_HANDLER, **params):
#     header_row = ['Date & Time', 'Object', 'Description']
#     if CONTROL_HANDLER.logger.is_empty():
#         table_data = [
#             ['N/A', 'N/A', 'N/A'],
#         ]
#     else:
#         table_data = CONTROL_HANDLER.logger.get_events_as_table(number = -1)
#     xml = scripts.get_custom_table(table_data, header_row)
#     return xml, 'text/html'
#
# def get_last_activity_information(SENSOR_HANDLER, CONTROL_HANDLER, **params):
#     header_row = ['Date & Time', 'Object', 'Description']
#     if CONTROL_HANDLER.logger.is_empty():
#         table_data = [
#             ['N/A', 'N/A', 'N/A'],
#         ]
#     else:
#         table_data = CONTROL_HANDLER.logger.get_events_as_table(number = 8)
#     xml = scripts.get_custom_table(table_data, header_row)
#     return xml, 'text/html'
#
# def get_active_sockets(SENSOR_HANDLER, CONTROL_HANDLER, **params):
#     d = CONTROL_HANDLER.return_active_sockets()
#     j = scripts.dict_to_json(d)
#     return j, 'application/json'

def set_control(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    CONTROL_HANDLER.add_control(**params)
    return 0

def get_control_info_table_by_socket(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    header_row = ['Atribute', 'Current value']
    table_data = CONTROL_HANDLER.control_info_by_socket(**params)
    xml = scripts.get_custom_table(table_data, header_row)
    return xml, 'text/html'

def modify_control(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    CONTROL_HANDLER.modify_control(**params)
    return 0

def delete_control(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    CONTROL_HANDLER.delete_control(**params)
    return 0

def get_control_trigger_condition_socket(SENSOR_HANDLER, CONTROL_HANDLER, **params):
    data = {'sockets':SENSOR_HANDLER.sockets, 'sensors': SENSOR_HANDLER.sensors}
    xml = scripts.return_control_trigger_condition_socket(data = data)
    return xml, 'text/html'
