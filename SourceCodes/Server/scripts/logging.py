'''
Created on 29/4/2016

@author: Emadariaga
'''
from collections import namedtuple


#===============================================================================
#----------------------------------------------------------- # BASE LOGGER CLASS
#===============================================================================

Event = namedtuple('event', ['time', 'object', 'description'])

# Logger handle to log events
class EventLogger(object):
    def __init__(self):
        self.events = []
        self.event_count = 0

    def log(self, time, obj, description):
        self.events = [(Event(time, obj, description))] + self.events
        self.event_count += 1

    def is_empty(self):
        return False if self.events else True

    def get_events_as_table(self, number):
        if number == -1:
            return [[str(e.time).split(".")[0], e.object, e.description] for e in self.events]
        else:
            return [[str(e.time).split(".")[0], e.object, e.description] for e in self.events[:number]]
