'''
Created on 20/4/2016

@author: Emadariaga

this module takes raw imput data and generates xml/json information for the GUI
'''
import json
from socket import socket


#===============================================================================
#----------------------------------------------------------------------- # UTILS
#===============================================================================

def dict_to_json(d):
    return json.dumps(d)

#===============================================================================
#------------------------------------------------------------- # GRAPH TEMPLATES
#===============================================================================

def donut_chart_template(data, resize = True, sensors = True):
    """ Returns a valid json with the apropriate format to create a morris donut chart
    """
    d = {}
    d['data'] = [{'label':l, 'value':v} for l, v in data.items()]
    d['resize'] = resize
    if sensors:
        d['colors'] = ["#4d4dff", "#0000b3", "#0000ff"]
    else:
        d['colors'] = ["#ff1a1a", "#990000", "#cc0000"]
    return json.dumps(d)


def line_graph_template(data, x_max = [0, 1], y_max = [0, 1]):
    """ returns a valid graph template with the corresponding data
    """
    j = {'element': 'morris-line-chart'}
    j_opts = { "series":{"lines":{"show":True }, "points":{ "show":True } }, "grid":{ "hoverable":True }, "yaxis":{ "min":y_max[0], "max":y_max[1] },
              "xaxis":{ "min":x_max[0], "max":x_max[1] }, "tooltip":True, "tooltipOpts":{ "content":"'%s' of %x.1 is %y.4", "shifts":{ "x":-60, "y":25 } } };
    j['data'] = [{'label':l, 'data':v} for l, v in data.items()]
    j['options'] = j_opts
    j['xkey'] = 'd'
    j['ykeys'] = ['visits']
    j['labels'] = ['Visits2']
    j['smooth'] = False
    j['resize'] = True
    return json.dumps(j)

#===============================================================================
#------------------------------------------------------------- # OTHER TEMPLATES
#===============================================================================

def get_custom_table(table_data, header_row, table_class = "table table-bordered table-hover table-striped"):
    """ Returns a table given a list of values and a header
    """

    # Verify row length matches
    for line in table_data:
        if len(header_row) != len(line):
            raise ValueError

    # Start table formating

    # Header
    table_header = "".join(["<th>%s</th>" % h for h in header_row])
    table_header = "<thead><tr>%s</tr></thead>" % table_header

    # Body
    table_body = ""
    for line in table_data:
        table_row = "".join(["<td>%s</td>" % str(h) for h in line])
        table_body += "<tr>%s</tr>" % table_row
    table_body = "<tbody>%s</tbody>" % table_body

    # finish table
    table = "<table class=%s>%s%s</table>" % (table_class, table_header, table_body)

    return table

def return_discriminated_sensor_usage(data):
    STYLE = {
        'Digital Sensors' : ["alert-success", "progress-bar-success"],
        'Analog Sensors' : ["alert-info", ""],
        'High Current Digital Sensors' : ["alert-warning", "progress-bar-warning"],
    }
    xml = ""
    for label, value in data.items():
        xml += '<div class="alert %s">' % STYLE[label][0]
        xml += '<strong> %s </strong> %s' % (label, " %d of %d sensors used" % (value[0], value[1]))
        xml += '<div class="progress"><div class="progress-bar %s" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: %s">' % (STYLE[label][1], (str(100 * value[0] / value[1]) + '%'))
        xml += '</div></div></div>'
    return xml

def return_discriminated_control_usage(data):
    STYLE = {
        'Digital Controls' : ["alert-success", "progress-bar-success"],
        'High Current Digital Controls' : ["alert-warning", "progress-bar-warning"],
    }
    xml = ""
    for label, value in data.items():
        xml += '<div class="alert %s">' % STYLE[label][0]
        xml += '<strong> %s </strong> %s' % (label, " %d of %d sensors used" % (value[0], value[1]))
        xml += '<div class="progress"><div class="progress-bar %s" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: %s">' % (STYLE[label][1], (str(100 * value[0] / value[1]) + '%'))
        xml += '</div></div></div>'
    return xml

def return_sensor_socket_usage(data):
    cells = []
    for s in data:
        cell = '<div class="row">'
        alert_style = 'danger' if not s['assigned'] else 'success'
        cell += '<div class="alert alert-%s">' % alert_style
        assigned = s['sensor_id'] if s['assigned'] else 'N/A'
        cell += '<strong> %s%d</strong></div></div>' % (s['type'], s['sk_num'])
        cells.append(cell)
    if len(cells) % 2:
        cells.append('<div class="row"></div>')
    row1 = "".join(cells[:len(cells) / 2])
    row2 = "".join(cells[len(cells) / 2:])
    xml = '<div class="col-lg-5">%s</div><div class="col-lg-2"></div><div class="col-lg-5">%s</div>' % (row1, row2)
    return xml


def return_control_socket_usage(data):
    cells = []
    for s in data:
        cell = '<div class="row">'
        alert_style = 'danger' if not s['assigned'] else 'success'
        cell += '<div class="alert alert-%s">' % alert_style
        assigned = s['control_id'] if s['assigned'] else 'N/A'
        cell += '<strong> %s%d</strong></div></div>' % (s['type'], s['sk_num'])
        cells.append(cell)
    if len(cells) % 2:
        cells.append('<div class="row"></div>')
    row1 = "".join(cells[:len(cells) / 2])
    row2 = "".join(cells[len(cells) / 2:])
    xml = '<div class="col-lg-5">%s</div><div class="col-lg-2"></div><div class="col-lg-5">%s</div>' % (row1, row2)
    return xml


def return_sensors_button_group(data):
    xml = ""
    for s in data['sockets']:
        xml += '<div class="btn-group" role="group">'
        if s.assigned:
            for sensor in data['sensors']:
                if s.sensor_id == sensor.id:
                    description = sensor.description
                    break
            button_class = 'success'
        else:
            button_class = 'warning disabled'
            description = "Undefined"

        label = '%s %d' % (s.type.upper(), s.number)
        xml += '<button type="button" class="btn btn-%s" id="sensor-button-group-%s" ' % (button_class, label.replace(" ", ""))
        xml += 'onClick="sensor_button_group_click(\'%s\', \'%s\')">%s</button>' % (label, description, label)
        xml += '</div>'
    return xml


def return_controls_group_and_status(data):
    xml = '<table class="table table-bordered table-hover table-striped">'
    xml += '<thead><tr><th>Socket</th><th>Name</th><th>Current status</th></tr></thead><tbody>'

    for s in data['sockets']:
        if s.assigned:
            for control in data['controls']:
                if s.control_id == control.id:
                    description = control.description
                    status = "ON" if int(control.status) else "OFF"
                    break
            button_class = 'success'
        else:
            button_class = 'warning'
            description = "Undefined"
            status = "Undefined"

        label = '%s %d' % (s.type.upper(), s.number)
        xml += '<tr class="%s">' % button_class
        xml += '    <td>%s</td>' % label
        xml += '    <td>%s</td>' % description
        xml += '    <td>%s</td>' % status
        xml += '</tr>'

    xml += '</tbody></table>'
    return xml


def return_controls_button_group(data):
    xml = ""
    for s in data['sockets']:
        xml += '<div class="btn-group" role="group">'
        if s.assigned:
            for control in data['controls']:
                if s.control_id == control.id:
                    description = control.description
                    break
            button_class = 'success'
        else:
            button_class = 'warning disabled'
            description = "Undefined"

        label = '%s %d' % (s.type.upper(), s.number)
        xml += '<button type="button" class="btn btn-%s" id="control-button-group-%s" ' % (button_class, label.replace(" ", ""))
        xml += 'onClick="control_button_group_click(\'%s\', \'%s\')">%s</button>' % (label, description, label)
        xml += '</div>'
    return xml
