'''
Created on 23-04-2016

@author: Edu
'''
from collections import namedtuple
from datetime import datetime
from random import randint
import threading


#===============================================================================
#--------------------------------------------------------- # MEASUREMENT HANDLER
#===============================================================================


# basic values argument
Socket = namedtuple('socket', ['number', 'control_id', 'type', 'assigned'])
SOCKET_TYPES = ['pwm', 'd', 'hcd']
SOCKET_MAP = [Socket(1, None, 'd', False),
             Socket(2, None, 'd', False),
             Socket(3, None, 'd', False),
             Socket(4, None, 'd', False),
             Socket(5, None, 'hcd', False),
             Socket(6, None, 'hcd', False),
            Socket(7, None, 'hcd', False),
            Socket(8, None, 'hcd', False),
             ]
STATUS_ON = 2
STATUS_OFF = 1


TRIGGER_TYPES = {
    'tt'  : "Time triggered",
    "sve" : "Triggered by sensor value",
    None : "Not specified",

}
TRIGGER_ACTION = {
    "1":"Switch state",
    "2":"Turn on for 1 sec then OFF",
    "3":"Turn on for 10 sec then OFF",
}

class ControlHandler(object):

    def __init__(self, max_pwm_controls = 2, max_digital_controls = 2, max_hcd_controls = 4, logger = None):
        self._inner_counter = 0
        self.controls = []
        self.sockets = SOCKET_MAP
        self.max_pwm_controls, self.max_digital_controls, self.max_hcd_controls = self.get_discriminated_max_controls()
        self.max_controls = max_hcd_controls + max_digital_controls + max_pwm_controls
        self.logger = logger
        self._current_status = [STATUS_OFF] * self.max_controls

    def get_discriminated_max_controls(self):
        data = [0] * len(SOCKET_TYPES)
        for s in SOCKET_MAP:
            for i, st in enumerate(SOCKET_TYPES):
                if s.type == st:
                    data[i] += 1
        return data

    def get_control_usage(self):
        current_status = []
        for s in self.sockets:
            if not s.assigned:
                current_status.append(STATUS_OFF)
            else:
                for c in self.controls:
                    if s.control_id == c.id:
                        if int(c.status):
                            current_status.append(STATUS_ON)
                        else:
                            current_status.append(STATUS_OFF)
                    break
        if len(current_status) < self.max_controls:
            print "ERROR: Could not build a correct control frame!"
            raise ValueError("ERROR: Could not build a correct control frame!")
        return current_status


    def get_control_current_status(self):
        online = 0
        offline = 0
        unassigned = len(SOCKET_MAP)
        for s in self.controls:
            unassigned -= 1
            if s.status:
                online += 1
            else:
                offline += 1
        return {'Offline':offline, 'Online':online, 'Not assigned':unassigned}

    def get_discriminated_control_usage(self):
        d = hcd = 0
        for s in self.controls:
            if s.type == 'd':
                d += 1
            elif s.type == 'hcd':
                hcd += 1
            else:
                raise ValueError("ERROR: Corrupted control!")

        return {'Digital Controls':[d, self.max_digital_controls],
                'High Current Digital Controls':[hcd, self.max_hcd_controls]}

    def get_control_socket_usage(self, **params):
        socket_status = []
        for s in self.sockets:
            socket_status.append({'assigned': s.assigned, 'type':s.type.upper(), 'sk_num':s.number, 'control_id':s.control_id})
        return socket_status

    def return_active_sockets(self, **params):
        active_socket_ids = []
        for s in self.sockets:
            if s.assigned:
                active_socket_ids.append('%s %d' % (s.type.upper(), s.number))
        return {'socket_ids':active_socket_ids}

    def control_info_by_socket(self, **params):
        socket_num = int(params['socket_val'].split()[1]) - 1
        if not self.sockets[socket_num].assigned:
            raise ValueError("ERROR: Socket is not yet assigned!")

        data = []
        for control in self.controls:
            if control.id == self.sockets[socket_num].control_id:
                data.append(['Name', control.description])
                data.append(['Description', control.additional_info])
                data.append(['Current status', "ON" if int(control.status) else "OFF"])
        return data

    def add_control(self, **params):
        if 'desc' not in params:
            raise ValueError("ERROR: control name is mandatory!")
        if params['socket_val'] == 'null':
            raise ValueError("ERROR: You must select a socket!")
        s_v = int(params['socket_val'])
        if params['type_val'] != self.sockets[s_v].type:
            raise ValueError("ERROR: control type does not correspond to socket type!")
        if self.sockets[s_v].assigned:
            raise ValueError("ERROR: Socket already taken!")
        if 'add_info' not in params:
            additional_info = "N/A"
        else:
            additional_info = params['add_info']

        control_params = {
            'id'                : self._inner_counter,
            'description'       : params['desc'],
            'additional_info'   : additional_info,
            'type'              : params['type_val'],
            'status'            : params['initial_status'],
        }

        self._inner_counter += 1
        self._current_status[s_v] = STATUS_ON

        control = Digitalcontrol(**control_params)

        self.controls = self.controls + [control]

        # if everything succeeded then:
        # take socket
        self.sockets[s_v] = self.sockets[s_v]._replace(assigned = True)
        self.sockets[s_v] = self.sockets[s_v]._replace(control_id = control_params['id'])
        logged_type = 'Analog' if control.type == 'a' else 'Digital'
        self.logger.log(datetime.now(), 'control', '%s control added on socket %d' % (logged_type, s_v + 1))

    def modify_control(self, **params):
        socket_num = int(params['socket_val'].split()[1]) - 1
        if not self.sockets[socket_num].assigned:
            raise ValueError("ERROR: Socket is not yet assigned!")

        for control in self.controls:
            if control.id == self.sockets[socket_num].control_id:
                setattr(control, 'status', params['value'])
                return

    def delete_control(self, **params):
        socket_num = int(params['socket_val'].split()[1]) - 1
        if not self.sockets[socket_num].assigned:
            raise ValueError("ERROR: Socket is not yet assigned!")
        for i, control in enumerate(self.controls):
            if control.id == self.sockets[socket_num].control_id:
                control.set_control_off()
                self.sockets[socket_num] = self.sockets[socket_num]._replace(assigned = False)
                self.sockets[socket_num] = self.sockets[socket_num]._replace(control_id = None)
                del self.controls[i]
                self._current_status[socket_num] = STATUS_OFF
                self.logger.log(datetime.now(), 'control', 'control deleted on socket %d' % (socket_num + 1))
                return


    def halt_measurements(self):
        for control in self.controls:
            control.set_control_off()
        self.logger.log(datetime.now(), "All controls", 'Halted all measurements')

    def save_env(self):
        """ Saves current environment information into a json file. Then it is loaded on next power on.
        """
        print ">>> SAVED ENV (control_modules.py 125) ???"
        pass


#===============================================================================
#-------------------------------------------------------------- # control CLASSES
#===============================================================================

# basic values argument
Measurement = namedtuple('measurement', ['time', 'value'])

class control(object):

    def __init__(self, **params):
        self.id = params['id']
        self.type = params['type']
        self.status = params['status']
        self._value_database = []


    def get_status(self):
        return self.status

    def set_control_on(self):
        if self.status:
            return
        self.status = True

    def set_control_off(self):
        if not self.status:
            return
        self.status = False

    def asdict(self):
        asdict = self.__dict__
        return asdict


class Digitalcontrol(control):

    def __init__(self, **params):
        if 'description' not in params:
            self.description = 'Undefined control Description'
        for param in params:
            setattr(self, param, params[param])
        super(Digitalcontrol, self).__init__(**params)




