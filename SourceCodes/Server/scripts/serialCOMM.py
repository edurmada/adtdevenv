# -*- coding: utf-8 -*-
'''
Created on 29/7/2016

@author: EASA12899
'''

import time
import serial
import numpy
from random import randint
from datetime import datetime
import random
import threading

class SerialPipe(object):

    def __init__(self, port, baudrate):
        self.serial = serial.Serial(port = port, baudrate = baudrate)
        self.enabled = False

    def open(self):
        while not self.serial.isOpen():
            pass
        self.enabled = True

    def serial_transaction(self, SENSOR_HANDLER, CONTROL_HANDLER):
        current_control_state = "".join([str(v) for v in CONTROL_HANDLER.get_control_usage()])
        self.serial.write(current_control_state)
        out = ''
        while not out:
            out = self.serial.readline()
        SENSOR_HANDLER.update_values(out)

    def _start_pipe(self, SENSOR_HANDLER, CONTROL_HANDLER):
        if self.enabled:
            self.serial_transaction(SENSOR_HANDLER, CONTROL_HANDLER)
            threading.Timer(0.1, self._start_pipe, [SENSOR_HANDLER, CONTROL_HANDLER]).start()

    def close(self):
        self.enabled = False
        time.sleep(1)
        self.serial.close()




if __name__ == "__main__":
    # configure the serial connections (the parameters differs on the device you are connecting to)
    ser = serial.Serial(
        port = 'COM6',
        baudrate = 115200,
        parity = serial.PARITY_ODD,
        stopbits = serial.STOPBITS_TWO,
        bytesize = serial.SEVENBITS
    )

    ser.isOpen()
    print "WAITING..."
    time.sleep(2)
    ser.flushInput()

    def generate_rand_byte():
        output = ""
        for _ in range(8):
            output += str(random.choice([1, 2]))
        return output


    for i in range(10):
        arr = generate_rand_byte()
        print "RPI: ", arr
        ser.write(arr)
        out = ''
        while not out:
            out = ser.readline()
            print "ARD: ", out
        time.sleep(1)
    ser.close()
