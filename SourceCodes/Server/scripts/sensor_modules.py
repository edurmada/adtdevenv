'''
Created on 23-04-2016

@author: Edu
'''
from collections import namedtuple
from datetime import datetime
from random import randint
import threading
import time
import types


#===============================================================================
#--------------------------------------------------------- # MEASUREMENT HANDLER
#===============================================================================


# basic values argument
Socket = namedtuple('socket', ['number', 'sensor_id', 'type', 'assigned'])
SOCKET_TYPES = ['a', 'd', 'hcd']
SOCKET_MAP = [Socket(1, None, 'a', False),
             Socket(2, None, 'a', False),
             Socket(3, None, 'a', False),
             Socket(4, None, 'a', False),
             Socket(5, None, 'd', False),
             Socket(6, None, 'd', False),
             Socket(7, None, 'd', False),
             Socket(8, None, 'hcd', False),
             ]

class SensorHandler(object):

    def __init__(self, logger = None):
        self._inner_counter = 0
        self.sensors = []
        self.sockets = SOCKET_MAP
        self.max_analog_sensors, self.max_digital_sensors, self.max_hcd_sensors = self.get_discriminated_max_sensors()
        self.max_sensors = self.max_hcd_sensors + self.max_digital_sensors + self.max_analog_sensors
        self.logger = logger

    def get_discriminated_max_sensors(self):
        data = [0] * len(SOCKET_TYPES)
        for s in SOCKET_MAP:
            for i, st in enumerate(SOCKET_TYPES):
                if s.type == st:
                    data[i] += 1
        return data

    def get_sensor_usage(self):
        offline = 0
        online = 0
        unassigned = len(SOCKET_MAP)
        for s in self.sensors:
            unassigned -= 1
            if s.status:
                online += 1
            else:
                offline += 1
        return {'Offline':offline, 'Online':online, 'Not assigned':unassigned}

    def get_discriminated_sensor_usage(self):
        d = a = hcd = 0
        for s in self.sensors:
            if s.type == 'd':
                d += 1
            elif s.type == 'a':
                a += 1
            elif s.type == 'hcd':
                hcd += 1
            else:
                raise ValueError("ERROR: Corrupted sensor!")

        return {'Analog Sensors':[a, self.max_analog_sensors], 'Digital Sensors':[d, self.max_digital_sensors],
                'High Current Digital Sensors':[hcd, self.max_hcd_sensors]}

    def get_sensor_socket_usage(self, **params):
        socket_status = []
        for s in self.sockets:
            socket_status.append({'assigned': s.assigned, 'type':s.type.upper(), 'sk_num':s.number, 'sensor_id':s.sensor_id})
        return socket_status

    def return_active_sockets(self, **params):
        active_socket_ids = []
        for s in self.sockets:
            if s.assigned:
                active_socket_ids.append('%s %d' % (s.type.upper(), s.number))
        return {'socket_ids':active_socket_ids}

    def sensor_info_by_socket(self, **params):
        socket_num = int(params['socket_val'].split()[1]) - 1
        if not self.sockets[socket_num].assigned:
            raise ValueError("ERROR: Socket is not yet assigned!")

        data = []
        for sensor in self.sensors:
            if sensor.id == self.sockets[socket_num].sensor_id:
                data.append(['Name', sensor.description])
                data.append(['Description', sensor.additional_info])
                data.append(['Status', 'ON' if sensor.status else 'OFF'])
                data.append(['Measure interval', sensor.measure_interval])
                data.append(['Save to file', 'YES' if sensor.save_to_file else 'NO'])
        return data


    def add_sensor(self, **params):
        if params['socket_val'] == 'null':
            raise ValueError("ERROR: You must select a socket!")
        s_v = int(params['socket_val'])
        if params['type_val'] != self.sockets[s_v].type:
            raise ValueError("ERROR: Sensor type does not correspond to socket type!")
        if self.sockets[s_v].assigned:
            raise ValueError("ERROR: Socket already taken!")
        if 'add_info' not in params:
            additional_info = "N/A"
        else:
            additional_info = params['add_info']
        if 'desc' not in params:
            raise ValueError("ERROR: Sensor name is mandatory!")

        # add sensor
        sensor_params = {
            'id'                : self._inner_counter,
            'additional_info'   : additional_info,
            'description'       : params['desc'],
            'type'              : params['type_val'],
            'status'            : params['status'],
            'measure_interval'  : params['measure_interval'],
            'save_to_file'      : params['save_to_file'],
        }
        self._inner_counter += 1
        if params['type_val'] == 'a':
            sensor = AnalogSensor(**sensor_params)
        else:
            sensor = DigitalSensor(**sensor_params)


        self.sensors = self.sensors + [sensor]

        # if everything succeeded then:
        # take socket
        self.sockets[s_v] = self.sockets[s_v]._replace(assigned = True)
        self.sockets[s_v] = self.sockets[s_v]._replace(sensor_id = sensor_params['id'])
        logged_type = 'Analog' if sensor.type == 'a' else 'Digital'
        self.logger.log(datetime.now(), 'Sensor', '%s sensor added on socket %d' % (logged_type, s_v + 1))

    def modify_sensor(self, **params):
        if 'value' not in params:
            raise ValueError("ERROR: You must specify the new value!")
        socket_num = int(params['socket_val'].split()[1]) - 1
        if not self.sockets[socket_num].assigned:
            raise ValueError("ERROR: Socket is not yet assigned!")

        for sensor in self.sensors:
            if sensor.id == self.sockets[socket_num].sensor_id:
                if str(getattr(sensor, params['tag'])) == str(params['value']):
                    raise ValueError("ERROR: The information provided needs to be different than the current!")
                if params['tag'] == 'save_to_file':
                    setattr(sensor, params['tag'], True if params['value'] == "True" else False)
                    action = 'Started' if params['value'] == "True" else 'Stopped'
                    self.logger.log(datetime.now(), 'Sensor', '%s saving to file on socket %d' % (action, socket_num + 1))
                elif params['tag'] == 'measure_interval':
                    self.logger.log(datetime.now(), 'Sensor', 'Modified measure interval from %d to %s on socket %d' % (getattr(sensor, params['tag']),
                                                                                                                        params['value'], socket_num + 1))
                    setattr(sensor, params['tag'], float(params['value']))
                elif params['tag'] == 'status' and params['value'] == "True":
                    # Set sensor ON if it was set ON
                    self.logger.log(datetime.now(), 'Sensor', 'Sensor was set ON on socket %d' % (socket_num + 1))
                    sensor.set_sensor_on()
                elif params['tag'] == 'status' and params['value'] != "True":
                    # Set sensor OFF if it was set OFF
                    self.logger.log(datetime.now(), 'Sensor', 'Sensor was set OFF on socket %d' % (socket_num + 1))
                    sensor.set_sensor_off()
                else:
                    setattr(sensor, params['tag'], params['value'])
                    self.logger.log(datetime.now(), 'Sensor', 'Modified sensor\'s %s to %s on socket %d' % (params['tag'], str(params['value']), socket_num + 1))
                return

    def delete_sensor(self, **params):
        socket_num = int(params['socket_val'].split()[1]) - 1
        if not self.sockets[socket_num].assigned:
            raise ValueError("ERROR: Socket is not yet assigned!")
        for i, sensor in enumerate(self.sensors):
            if sensor.id == self.sockets[socket_num].sensor_id:
                sensor.set_sensor_off()
                self.sockets[socket_num] = self.sockets[socket_num]._replace(assigned = False)
                self.sockets[socket_num] = self.sockets[socket_num]._replace(sensor_id = None)
                del self.sensors[i]
                self.logger.log(datetime.now(), 'Sensor', 'Sensor deleted on socket %d' % (socket_num + 1))
                return


    def halt_measurements(self):
        for sensor in self.sensors:
            sensor.set_sensor_off()
        self.logger.log(datetime.now(), "All sensors", 'Halted all measurements')

    def update_values(self, raw_data):
        raw_data = raw_data.split(";")
        time = datetime.now()
        for i, s in enumerate(SOCKET_MAP):
            if s.assigned:
                for sensor in self.sensors:
                    if sensor.id == s.sensor_id:
                        sensor.last_measurement = raw_data[i]
                        sensor.last_measurement_time = time

    def save_env(self):
        """ Saves current environment information into a json file. Then it is loaded on next power on.
        """
        print ">>> SAVED ENV (sensor_modules.py 125) ???"
        pass


#===============================================================================
#-------------------------------------------------------------- # SENSOR CLASSES
#===============================================================================

# basic values argument
Measurement = namedtuple('measurement', ['time', 'value'])

class Sensor(object):

    def __init__(self, **params):
        self.id = params['id']
        self.type = params['type']
        self.status = True if params['status'] == "True" else False
        self.measure_interval = float(params['measure_interval'])
        self.last_measurement = 0
        self.last_measurement_time = datetime.now()
        self._value_database = []
        self.save_to_file = True if params['save_to_file'] == "True" else False
        if self.status:
            self._measure()

    def get_value(self):
        aux_measurement = Measurement(self.last_measurement_time, self.last_measurement)
        self._value_database.append(aux_measurement)
        return aux_measurement

    def get_history(self, samples = 50):
        if not self._value_database:
            return {'values':[[0, 0]], 'xmin':0, 'xmax':10, 'step':1, 'ymin':0, 'ymax':1}
        values = []
        index = []
        start = 0 if len(self._value_database) < samples else len(self._value_database) - samples
        ymax = ymin = self._value_database[start].value
        xmin = -(len(self._value_database) if len(self._value_database) < samples else samples) * self.measure_interval
        for i, m in enumerate(self._value_database[start:]):
            values.append(m.value)
            index.append(i * self.measure_interval)
            if self.type != 'a':
                values.append(m.value)
                index.append(i * self.measure_interval + 0.99 * self.measure_interval)
            ymax = m.value if m.value > ymax else ymax
            ymin = m.value if m.value < ymin else ymin
        # Turn arround x axis
        aux_index = []
        for i in range(len(values)):
            aux_index.append(-index[-1 - i])
        # Zip values and index
        history = zip(aux_index, values)
        dif = (ymax - ymin)
        ymax = ymax + 0.1 * dif
        ymin = ymin - 0.1 * dif
        return {'values':history, 'xmin':xmin, 'xmax':0, 'step':self.measure_interval, 'ymin':ymin, 'ymax':ymax}


    def get_status(self):
        return self.status

    def set_sensor_on(self):
        if self.status:
            return
        self.status = True
        self._measure()

    def set_sensor_off(self):
        if not self.status:
            return
        self.status = False

    def _measure(self):
        if self.status:
            self.get_value()
            threading.Timer(float(self.measure_interval), self._measure).start()

    def asdict(self):
        asdict = self.__dict__
        return asdict


class AnalogSensor(Sensor):

    def __init__(self, **params):
        if 'description' not in params:
            self.description = 'Undefined Sensor Description'
        for param in params:
            setattr(self, param, params[param])
        super(AnalogSensor, self).__init__(**params)

    def get_value(self):
        # There should be an overdrive to establish correct measure ie... thermometer
        aux_measurement = Measurement(self.last_measurement_time, float(self.last_measurement))
        self._value_database.append(aux_measurement)
        return aux_measurement

class DigitalSensor(Sensor):

    def __init__(self, **params):
        if 'description' not in params:
            self.description = 'Undefined Sensor Description'
        for param in params:
            setattr(self, param, params[param])
        super(DigitalSensor, self).__init__(**params)

    def get_value(self):
        # There should be an overdrive to establish correct measure ie... tilt switch
        aux_measurement = Measurement(self.last_measurement_time, int(self.last_measurement))
        self._value_database.append(aux_measurement)
        return aux_measurement

if __name__ == '__main__':
    print('creating sensor handler')
    params = {
        'id': "th01",
        'description': "Thermometer 1",
        'type': 'a',
        'status': False,
        'measure_interval': 0.5,
    }
    th = AnalogSensor(**params)

    print("Sensor params")
    print th.asdict()

