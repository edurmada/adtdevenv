// ===========================================================================================================
// 											CONFIG SENSORS JS
// ===========================================================================================================

function display_console_message(console, status, message){
	$(console+"-div").removeClass();
	if (status){
		$(console+"-div").addClass('form-group has-error');
		$(console+"-box").html(message);
	}
	else{
		$(console+"-div").addClass('form-group has-success');
		$(console+"-box").html("SUCCESS!");
	}
}


function sensor_add_button_click() {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 && xhr.status == 200) {
			display_console_message("#sensor-add-response", xhr.responseText != '', xhr.responseText);
			return
		}
	}
	socket_val = $("#sensor-socket-value").val();
	type_val   = $("#sensor-type-value").val();
	desc       = $("#sensor-description").val();
	add_info   = $("#sensor-aditional-info").val();
	status   = $("#sensor-status-value").val();
	measure_interval = $("#sensor-measure-interval-value").val();
	save_to_file = $("#sensor-savefile-value").val();

	var sensor_data = {'method':'set_sensor','socket_val':socket_val, 'type_val':type_val, 'desc':desc, 
			'add_info':add_info, 'status':status, 'measure_interval':measure_interval, 
			'save_to_file': save_to_file};
	var str = [];
	for(var p in sensor_data)
	    if (sensor_data.hasOwnProperty(p)) {
	        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(sensor_data[p]));
	    }
	str = str.join("&");
	xhr.open("POST", "pylib.py?" + str, true);
	xhr.send();
}


function get_discriminated_sensor_data(){
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
	  if (xhr.readyState == 4 && xhr.status == 200) {
		  $('#sensor-discriminated-info').html(xhr.responseText);
	  }
	};

	xhr.open("GET", "pylib.py?method=get_discriminated_sensor_data", true);
	xhr.send();
}

function get_sensor_socket_usage(){
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
	  if (xhr.readyState == 4 && xhr.status == 200) {
		  $('#socket-discriminated-info').html(xhr.responseText);
	  }
	};

	xhr.open("GET", "pylib.py?method=get_sensor_socket_usage", true);
	xhr.send();
}

CURRENT_INNER_NVBAR = 'INFORMATION'
function sensors_inner_navbar(){
	$("#sensors-inner-navbar-information").click(function(){
		CURRENT_INNER_NVBAR = 'INFORMATION'
		// goto info panel
		sensor_navbar_change("#sensors-inner-navbar-information");
		// Get graphs' info
		get_sensor_status();
		get_discriminated_sensor_data();
		get_sensor_socket_usage();
	});
	$("#sensors-inner-navbar-add").click(function(){
		CURRENT_INNER_NVBAR = 'ADD'
		sensor_navbar_change("#sensors-inner-navbar-add");
	});
	$("#sensors-inner-navbar-modify").click(function(){
		CURRENT_INNER_NVBAR = 'MODIFY'
		sensor_navbar_change("#sensors-inner-navbar-modify");
		get_sensor_button_group();
		modify_sensor();
	});
	
}

function sensor_navbar_change(obj){
	var all_obj = ["#sensors-inner-navbar-information",
	               "#sensors-inner-navbar-options",
	               "#sensors-inner-navbar-add",
	               "#sensors-inner-navbar-modify",
	               "#sensors-inner-navbar-delete"]
	for (i=0; i<all_obj.length;i++){
		if (all_obj[i] === obj){
			$(all_obj[i]).addClass('active');
			$(all_obj[i]+"-panel").show();
		}
		else{
			$(all_obj[i]).removeClass();
			$(all_obj[i]+"-panel").hide();
		}
	}
};

function disable_modify_buttons(){
	var all_buttons = ["#btn-delete-sensor",
	                   "#btn-modify-savefile-value",
	                   "#btn-modify-measure-interval-value",
	                   "#btn-modify-status-value",
	                   "#btn-modify-aditional-info",
	                   "#btn-modify-sensor-description"]
	for (i=0; i<all_buttons.length;i++){
		$(all_buttons[i]).addClass('disabled');
	}
}

function enable_modify_buttons(){
	var all_buttons = ["#btn-delete-sensor",
	                   "#btn-modify-savefile-value",
	                   "#btn-modify-measure-interval-value",
	                   "#btn-modify-status-value",
	                   "#btn-modify-aditional-info",
	                   "#btn-modify-sensor-description"]
	for (i=0; i<all_buttons.length;i++){
		$(all_buttons[i]).removeClass('disabled');
	}
}

function get_sensor_button_group() {
	$('#graph-content').hide();
	$('#graph-tooltip-content').html("Please select a sensor to show its last readings");
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 && xhr.status == 200) {
			$('#sensor-button-group').html(xhr.responseText);
		}
	}
	disable_modify_buttons();
	xhr.open("GET", "pylib.py?method=get_sensor_button_group", true);
	xhr.send();
}

SENSOR_SELECTED = "";

function sensor_button_group_click(clicked_id, description)
{
	if (SENSOR_SELECTED === clicked_id){
		return;
	}
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 && xhr.status == 200) {
			if(xhr.responseText.match(/^ERROR/)){
				display_console_message("#sensor-modify-response", xhr.responseText != '', xhr.responseText + '. If you are having trouble modifying values please refer to the explanation section below');
				SENSOR_SELECTED = "";
				disable_modify_buttons();
				$('#modify-tooltip').html("No sensor selected");
			}
			else{
				display_console_message("#sensor-modify-response", 0, 0);
				SENSOR_SELECTED = clicked_id;
				enable_modify_buttons();
				$('#modify-tooltip').html(xhr.responseText);
			}
			return
		}
	}
	var sensor_data = {'method':'get_sensor_info_table_by_socket','socket_val':clicked_id};
	var str = [];
	for(var p in sensor_data)
	    if (sensor_data.hasOwnProperty(p)) {
	        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(sensor_data[p]));
	    }
	str = str.join("&");
	xhr.open("GET", "pylib.py?" + str, true);
	xhr.send();


}

function delete_sensor (){
	if (SENSOR_SELECTED === ""){
		return;
	}
	if ($("#delete-sensor").prop('checked')){
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function(){
			if (xhr.readyState == 4 && xhr.status == 200) {
				if(xhr.responseText.match(/^ERROR/)){
					display_console_message("#sensor-modify-response", xhr.responseText != '', xhr.responseText + '. If you are having trouble modifying values please refer to the explanation section below');
				}
				else{
					var aux_var = SENSOR_SELECTED
					SENSOR_SELECTED = "";
					get_sensor_button_group();
					$("#modify-tooltip").html("No sensor selected")
				}
			}
		}
		var sensor_data = {'method':'delete_sensor', 'socket_val':SENSOR_SELECTED};
		var str = [];
		for(var p in sensor_data)
		    if (sensor_data.hasOwnProperty(p)) {
		        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(sensor_data[p]));
		    }
		str = str.join("&");
		xhr.open("POST", "pylib.py?" + str, true);
		xhr.send();
	}
	else {
		display_console_message("#sensor-modify-response", !$("#delete-sensor").prop('checked'), 'ERROR: You need to check to verify intent. If you are having trouble modifying values please refer to the explanation section below');
	}
}

function modify_value(tag, obj){
	if (SENSOR_SELECTED === ""){return}
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 && xhr.status == 200) {
			if(xhr.responseText.match(/^ERROR/)){
				display_console_message("#sensor-modify-response", xhr.responseText != '', xhr.responseText + '. If you are having trouble modifying values please refer to the explanation section below');
			}
			else{
				var aux_var = SENSOR_SELECTED
				SENSOR_SELECTED = "";
				sensor_button_group_click(aux_var, 'N/A');
			}
		}
	}
	var sensor_data = {'method':'modify_sensor', tag: tag,  value:$(obj).val(), 'socket_val':SENSOR_SELECTED};
	var str = [];
	for(var p in sensor_data)
	    if (sensor_data.hasOwnProperty(p)) {
	        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(sensor_data[p]));
	    }
	str = str.join("&");
	xhr.open("POST", "pylib.py?" + str, true);
	xhr.send();
}
