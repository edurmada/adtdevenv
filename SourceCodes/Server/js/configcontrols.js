// ===========================================================================================================
// 											CONFIG controlS JS
// ===========================================================================================================

function display_console_message(console, status, message){
	$(console+"-div").removeClass();
	if (status){
		$(console+"-div").addClass('form-group has-error');
		$(console+"-box").html(message);
	}
	else{
		$(console+"-div").addClass('form-group has-success');
		$(console+"-box").html("SUCCESS!");
	}
}

function control_add_button_click() {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 && xhr.status == 200) {
			display_console_message("#control-add-response", xhr.responseText != '', xhr.responseText);
			get_control_group_and_status();
			return
		}
	}
	// Basic info
	description= $("#control-description").val();
	add_info   = $("#control-aditional-info").val();
	type_val   = $("#control-type-value").val();
	socket_val = $("#control-socket-value").val();
	initial_status = $("#control-initial-status").val();
	var control_data = {'method':'set_control','socket_val':socket_val, 'type_val':type_val, 'desc':description, 
			'add_info':add_info, 'initial_status':initial_status}
	var str = [];
	for(var p in control_data)
	    if (control_data.hasOwnProperty(p)) {
	        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(control_data[p]));
	    }
	str = str.join("&");
	xhr.open("POST", "pylib.py?" + str, true);
	xhr.send();
}

function get_discriminated_control_data(){
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
	  if (xhr.readyState == 4 && xhr.status == 200) {
		  $('#control-discriminated-info').html(xhr.responseText);
	  }
	};

	xhr.open("GET", "pylib.py?method=get_discriminated_control_data", true);
	xhr.send();
}

function get_control_trigger_condition_socket(){
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
	  if (xhr.readyState == 4 && xhr.status == 200) {
		  $('#control-trigger-condition-socket').html(xhr.responseText);
	  }
	};

	xhr.open("GET", "pylib.py?method=get_control_trigger_condition_socket", true);
	xhr.send();
}

function get_control_socket_usage(){
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
	  if (xhr.readyState == 4 && xhr.status == 200) {
		  $('#socket-discriminated-info').html(xhr.responseText);
	  }
	};

	xhr.open("GET", "pylib.py?method=get_control_socket_usage", true);
	xhr.send();
}

CURRENT_INNER_NVBAR = 'INFORMATION'
function controls_inner_navbar(){
	$("#controls-inner-navbar-information").click(function(){
		CURRENT_INNER_NVBAR = 'INFORMATION'
		// goto info panel
		control_navbar_change("#controls-inner-navbar-information");
		// Get graphs' info
		get_control_status();
		get_discriminated_control_data();
		get_control_socket_usage();
	});
	$("#controls-inner-navbar-add").click(function(){
		CURRENT_INNER_NVBAR = 'ADD'
		control_navbar_change("#controls-inner-navbar-add");
		get_control_group_and_status();
	});
	$("#controls-inner-navbar-modify").click(function(){
		CURRENT_INNER_NVBAR = 'MODIFY'
		control_navbar_change("#controls-inner-navbar-modify");
		get_control_button_group();
	});
	
}

function control_navbar_change(obj){
	var all_obj = ["#controls-inner-navbar-add",
	               "#controls-inner-navbar-modify",
	               "#controls-inner-navbar-information"]
	for (i=0; i<all_obj.length;i++){
		if (all_obj[i] === obj){
			$(all_obj[i]).addClass('active');
			$(all_obj[i]+"-panel").show();
		}
		else{
			$(all_obj[i]).removeClass();
			$(all_obj[i]+"-panel").hide();
		}
	}
};


function disable_modify_buttons(){
	var all_buttons = ["#btn-delete-control",
	                   "#btn-modify-sensor-on",
	                   "#btn-modify-sensor-off"]
	for (i=0; i<all_buttons.length;i++){
		$(all_buttons[i]).addClass('disabled');
	}
}

function enable_modify_buttons(){
	var all_buttons = ["#btn-delete-control",
	                   "#btn-modify-sensor-on",
	                   "#btn-modify-sensor-off"]
	for (i=0; i<all_buttons.length;i++){
		$(all_buttons[i]).removeClass('disabled');
	}
}

function get_control_group_and_status() {
	$('#graph-content').hide();
	$('#graph-tooltip-content').html("Please select a control to show its last readings");
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 && xhr.status == 200) {
			$('#control-button-group-and-status').html(xhr.responseText);
		}
	}
	disable_modify_buttons();
	xhr.open("GET", "pylib.py?method=get_control_group_and_status", true);
	xhr.send();
}


function get_control_button_group() {
	$('#graph-content').hide();
	$('#graph-tooltip-content').html("Please select a control to show its last readings");
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 && xhr.status == 200) {
			$('#control-button-group').html(xhr.responseText);
		}
	}
	disable_modify_buttons();
	xhr.open("GET", "pylib.py?method=get_control_button_group", true);
	xhr.send();
}

CONTROL_SELECTED = "";

function control_button_group_click(clicked_id, description)
{
	if (CONTROL_SELECTED === clicked_id){
		return;
	}
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 && xhr.status == 200) {
			if(xhr.responseText.match(/^ERROR/)){
				display_console_message("#control-modify-response", xhr.responseText != '', xhr.responseText + '. If you are having trouble modifying values please refer to the explanation section below');
				CONTROL_SELECTED = "";
				disable_modify_buttons();
				$('#modify-tooltip').html("No control selected");
			}
			else{
				display_console_message("#control-modify-response", 0, 0);
				CONTROL_SELECTED = clicked_id;
				enable_modify_buttons();
				$('#modify-tooltip').html(xhr.responseText);
			}
			return
		}
	}
	var control_data = {'method':'get_control_info_table_by_socket','socket_val':clicked_id};
	var str = [];
	for(var p in control_data)
	    if (control_data.hasOwnProperty(p)) {
	        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(control_data[p]));
	    }
	str = str.join("&");
	xhr.open("GET", "pylib.py?" + str, true);
	xhr.send();


}

function delete_control (){
	if (CONTROL_SELECTED === ""){
		return;
	}
	if ($("#delete-control").prop('checked')){
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function(){
			if (xhr.readyState == 4 && xhr.status == 200) {
				if(xhr.responseText.match(/^ERROR/)){
					display_console_message("#control-modify-response", xhr.responseText != '', xhr.responseText + '. If you are having trouble modifying values please refer to the explanation section below');
				}
				else{
					var aux_var = CONTROL_SELECTED
					CONTROL_SELECTED = "";
					get_control_button_group();
					$("#modify-tooltip").html("No control selected")
				}
			}
		}
		var control_data = {'method':'delete_control', 'socket_val':CONTROL_SELECTED};
		var str = [];
		for(var p in control_data)
		    if (control_data.hasOwnProperty(p)) {
		        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(control_data[p]));
		    }
		str = str.join("&");
		xhr.open("POST", "pylib.py?" + str, true);
		xhr.send();
	}
	else {
		display_console_message("#control-modify-response", !$("#delete-control").prop('checked'), 'ERROR: You need to check to verify intent. If you are having trouble modifying values please refer to the explanation section below');
	}
}

function modify_value(tag, value){
	if (CONTROL_SELECTED === ""){return}
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 && xhr.status == 200) {
			if(xhr.responseText.match(/^ERROR/)){
				display_console_message("#control-modify-response", xhr.responseText != '', xhr.responseText + '. If you are having trouble modifying values please refer to the explanation section below');
			}
			else{
				var aux_var = CONTROL_SELECTED
				CONTROL_SELECTED = "";
				control_button_group_click(aux_var, 'N/A');
			}
		}
	}
	var control_data = {'method':'modify_control', tag: tag,  value:value, 'socket_val':CONTROL_SELECTED};
	var str = [];
	for(var p in control_data)
	    if (control_data.hasOwnProperty(p)) {
	        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(control_data[p]));
	    }
	str = str.join("&");
	xhr.open("POST", "pylib.py?" + str, true);
	xhr.send();
}


function inhover(){
	alert("this works");
}