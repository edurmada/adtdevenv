//===========================================================================================================
//												QUERRY JS
//===========================================================================================================

function display_console_message(console, status, message){
	$(console+"-div").removeClass();
	if (status){
		$(console+"-div").addClass('form-group has-error');
	}
	else{
		$(console+"-div").addClass('form-group has-success');
	}
	$(console+"-box").html(message);
}

function get_sensor_graph_tooltip(clicked_id)
{
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 && xhr.status == 200) {
			if(xhr.responseText.match(/^ERROR/)){
				display_console_message("#sensor-querie-response", xhr.responseText != '', xhr.responseText + '. If you are having trouble modifying values please refer to the explanation section below');
			}
			else{
				$('#sensor-current-values').html(xhr.responseText);
			}
			return
		}
	}
	var sensor_data = {'method':'get_sensor_info_table_by_socket','socket_val':clicked_id};
	var str = [];
	for(var p in sensor_data)
	    if (sensor_data.hasOwnProperty(p)) {
	        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(sensor_data[p]));
	    }
	str = str.join("&");
	xhr.open("GET", "pylib.py?" + str, true);
	xhr.send();


}


//Flot Line Chart with Tooltips
function get_sensor_data_graph(clicked_id) {
	if (SENSOR_SELECTED !== clicked_id){
		return
	}
	var timeout = $("#sensor-graph-refresh-rate").val() 
	var samples = $("#sensor-graph-samples").val()
	var freeze_graph = $("#sensor-freeze-graph").prop('checked')
	
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 && xhr.status == 200) {
			if (xhr.responseText == "ERROR: no sensors set yet"){
				alert("Empty response");
				return
			}
			var json = jQuery.parseJSON(xhr.responseText);
			if (!freeze_graph){
				plot();
			}
			function plot() {
				var plotObj = $.plot($("#graph-content"), json.data, json.options);
			}
			get_sensor_graph_tooltip(clicked_id);
			setTimeout(get_sensor_data_graph, timeout, clicked_id);
		}
	}
	var sensor_data = {'method':'get_graph_data','socket_id':clicked_id, 'samples':samples};
	var str = [];
	for(var p in sensor_data)
	    if (sensor_data.hasOwnProperty(p)) {
	        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(sensor_data[p]));
	    }
	str = str.join("&");
	xhr.open("GET", "pylib.py?" + str, true);
	xhr.send();
}


function get_sensor_button_group() {
	$('#graph-content').hide();
//	$('#graph-message-box').html("Please select a sensor to show its last readings");
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 && xhr.status == 200) {
			$('#queies-sensor-button-group').html(xhr.responseText);
		}
	}
	xhr.open("GET", "pylib.py?method=get_sensor_button_group", true);
	xhr.send();
}

SENSOR_SELECTED = ""

function sensor_button_group_click(clicked_id, description)
{
	if (SENSOR_SELECTED === clicked_id){
		return
	}
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 && xhr.status == 200) {
			var json = jQuery.parseJSON(xhr.responseText);
			valid_ids = json.socket_ids;
			for(i=0;i<valid_ids.length;i++){
				if (clicked_id == valid_ids[i]) {
					SENSOR_SELECTED = clicked_id;
					get_sensor_data_graph(clicked_id);
					$('#graph-content').show();
					$('#graph-title').html("Last sensor measurements through Socket " + clicked_id + ": <strong>" + description + "</strong>");
					display_console_message("#sensor-querie-response", false, "Correctly selected a sensor!")
					return
				}
			}
			display_console_message("#sensor-querie-response", true, "ERROR: Socket selected is not assigned!")
		}
	}
	xhr.open("GET", "pylib.py?method=get_active_sockets", true);
	xhr.send();

}
