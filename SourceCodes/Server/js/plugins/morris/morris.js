/* @license
morris.js v0.5.0
Copyright 2014 Olly Smith All rights reserved.
Licensed under the BSD-2-Clause License.
*/


Morris.Donut({
  element: 'chart',
  data: [
    {label: "Credit", value: 12},
    {label: "Debit", value: 30},
    {label: "Loan", value: 20}
  ]
});