
// ===========================================================================================================
// 												PUBLIC  JS
// ===========================================================================================================

// Donut Chart
function get_sensor_status() {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
	  if (xhr.readyState == 4 && xhr.status == 200) {
		  $('#morris-sensors-chart').html('');
		  var jsonResponse = JSON.parse(xhr.responseText);
		  jsonResponse['element'] = 'morris-sensors-chart';
		  Morris.Donut(jsonResponse);
	  }
	};

	xhr.open("GET", "pylib.py?method=get_sensor_data", true);
	xhr.send();
}

//Donut Chart
function get_control_status() {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
	  if (xhr.readyState == 4 && xhr.status == 200) {
		  $('#morris-control-chart').html('');
		  var jsonResponse = JSON.parse(xhr.responseText);
		  jsonResponse['element'] = 'morris-control-chart';
		  Morris.Donut(jsonResponse);
	  }
	};

	xhr.open("GET", "pylib.py?method=get_control_data", true);
	xhr.send();
}


// ===========================================================================================================
// 												HOME JS
// ===========================================================================================================

function get_last_activity_information() {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(){
		if (xhr.readyState == 4 && xhr.status == 200) {
			$('#last-activity-information').html(xhr.responseText);
			setTimeout(get_last_activity_information, 3000);
		}
	}
	xhr.open("GET", "pylib.py?method=get_last_activity_information", true);
	xhr.send();
}




