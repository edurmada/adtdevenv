#include <String.h>
#include "library.cpp"

/*
 * Define control and sensor pins:
 */


/*
 * Analyze incomming frames:
 */

SerialInputFrame SerialFrameFromString(String input)
{
  SerialInputFrame frame;
  char aux;
  int input_length = input.length();
  input.toCharArray(frame.control_information, 10);
  if (input_length != 8)
  {
    frame.frame_status = 1;
  }
  else
  {
    for (int i = 0; i < 8; i++)
    {
      aux = frame.control_information[i];
      if (aux!='2' && aux!='1')
      {
        frame.frame_status = 2 + i;
        return frame;
      }
    }
  }
  return frame;
}


/*
 * General purpose library:
 */
 
void PrintToSerial(String string)
{
  Serial.println(string);
}

int CONTROL_PIN_NUMBERS[] = {13,12,11,10,9,8,7,6};

void ConfigControlPins()
{
  for (int i=0; i < 8; i++)
  {
    pinMode(CONTROL_PIN_NUMBERS[i], OUTPUT);
    digitalWrite(CONTROL_PIN_NUMBERS[i], LOW);
  }
}

void SetControlPins(char cc[8])
{
  for (int i=0; i < 8; i++)
  {
    if (cc[i]=='1')
    {
      digitalWrite(CONTROL_PIN_NUMBERS[i], LOW);
    }
    if (cc[i]=='2')
    {
      digitalWrite(CONTROL_PIN_NUMBERS[i], HIGH);
    }
  }
}

int SENSOR_PIN_NUMBERS[] = {A3,A2,A1,A0,5,4,3,2};

void ConfigSensorPins()
{
  for (int i=0; i < 8; i++)
  {
    pinMode(SENSOR_PIN_NUMBERS[i], INPUT);
  }
}

String GetSensorInfo()
{
  String output = "";
  for (int i=0; i < 4; i++)
  {
    output += analogRead(SENSOR_PIN_NUMBERS[i]);
    output += ";";
  }
  for (int i=4; i < 8; i++)
  {
    output += digitalRead(SENSOR_PIN_NUMBERS[i]);
    output += ";";
  }
  return output;
}

/*
 * Main CODE:
 */

void setup()
{
  Serial.begin(115200);
  while (! Serial); // Wait until Serial is ready - Leonardo
  PrintToSerial("R");
  ConfigControlPins();
  ConfigSensorPins();
}

void loop() {
  if (Serial.available())
  {
    String output;
    String input = Serial.readString();
    SerialInputFrame frame = SerialFrameFromString(input);
    if (frame.frame_status)
    {
      output = "E: " + String(frame.frame_status) + frame.control_information;
    }
    else
    {
      SetControlPins(frame.control_information);
      String sensorInfo = GetSensorInfo();
      output = sensorInfo ;
    }
    
    //PrintToSerial(frame.control_information);
    PrintToSerial(output);
  }

}

