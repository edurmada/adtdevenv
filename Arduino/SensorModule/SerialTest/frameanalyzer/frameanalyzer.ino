#include <String.h>
#include "library.cpp"

SerialFrame SerialFrameFromString(String input)
{
  SerialInputFrame frame;
  int input_length = input.length();
  int status_flag = 0; // saves the status of the frame analisis
  char aux_char; // stores current char form input
  int i, j = 0, aux_control;
  for (i = 0; i < input_length; i++)
  {
    aux_char = input.charAt(i);
    if (aux_char == ';')
    {
      status_flag += 1;
      j = 0;
      continue;
    }
    switch (status_flag) {
      case 0:
        frame.request_type[j] = aux_char;
        break;
      case 1:
        frame.port_type[j] = aux_char;
        break;
      case 2:
        frame.port_number[j] = aux_char;
        break;
      case 3:
        frame.value[j] = aux_char;
        break;
      default:
        frame.frame_status = 'E';
        break;
    }
    j++;
  }
  if (frame.frame_status != 'E')
  {
    frame.frame_status = 'O';
  }
  return frame;
}

void PrintToSerial(String string)
{
  Serial.println(string);
}

void setup()
{
  Serial.begin(115200);
  while (! Serial); // Wait until Serial is ready - Leonardo
  Serial.println("Redy to recieve comands!");
  Serial.println("req type; port type; port num; val;");
}

void loop() {
  if (Serial.available())
  {
    String input = Serial.readString();
    SerialFrame frame = SerialFrameFromString(input);
    PrintToSerial("Recieved: " + input);
    PrintToSerial("Port type: " + String(frame.request_type));
    PrintToSerial("request type: " + String(frame.port_type));
    PrintToSerial("Port number: " + String(frame.port_number));
    PrintToSerial("Value: " + String(frame.value));
    PrintToSerial("Status: " + String(frame.frame_status));
  }

}

