#include <String.h>
using namespace std;

class SerialInputFrame
{
  public:
    char control_information[10] = ""; // either "G" get or "S" set
    char port_type[5] = "";    // either "A" analog or "Digital"
    char port_number[10] = "";   // number of port to be set
    char value[10] = "";
    char frame_status = ' '; // saves status of frame "EMPTY", "ERROR" or "NO_ERROR".
};


