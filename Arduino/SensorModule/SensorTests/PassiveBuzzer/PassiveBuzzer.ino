
int val = 0;
int buzzer=11;


void setup()
{
  Serial.begin(9600);
  while (! Serial); // Wait until Serial is ready - Leonardo
  Serial.println("Testing buffer");
  pinMode(buzzer,OUTPUT);
}

void loop() {
  unsigned char i,j;
  Serial.println("Serial at 25%");
  for(i=0;i<100;i++)
  {
    digitalWrite(buzzer,HIGH);
    delay(1);
    digitalWrite(buzzer,LOW);
    delay(3);
  }
  Serial.println("Serial at 50%");
  for(i=0;i<100;i++)
  {
    digitalWrite(buzzer,HIGH);
    delay(2);
    digitalWrite(buzzer,LOW);
    delay(2);
  }
  Serial.println("Serial at 75%");
  for(i=0;i<100;i++)
  {
    digitalWrite(buzzer,HIGH);
    delay(3);
    digitalWrite(buzzer,LOW);
    delay(1);
  }
  Serial.println("Serial from 0% to 100%");
  for(i=0;i<255;i++)
  {
    analogWrite(buzzer,i);
    delay(50);
  }

}
