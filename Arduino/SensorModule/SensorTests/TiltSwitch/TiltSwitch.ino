
int tiltswitch = 11;                                              // tilt switch pin
int val = 0;                                                      // variable to store the read value

void setup()
{
  Serial.begin(9600);
  while (! Serial);                                               // Wait until Serial is ready - Leonardo
  Serial.println("Serial port opened corectly!");
  pinMode(tiltswitch, INPUT);
}

void loop() {
  val = digitalRead(tiltswitch);
  Serial.print("Value read: ");
  Serial.println(val);
  delay(1000);
}
