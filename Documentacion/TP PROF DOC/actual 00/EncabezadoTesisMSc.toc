\contentsline {chapter}{\hbox to\@tempdima {\hfil }Resumen}{\es@scroman {vii}}
\select@language {spanish}
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Lista de figuras}{\es@scroman {ix}}
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Lista de tablas}{\es@scroman {xi}}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}
\contentsline {chapter}{\numberline {2}Evaluaci\IeC {\'o}n de las soluciones existentes y la propuesta}{3}
\contentsline {section}{\numberline {2.1}Control}{3}
\contentsline {section}{\numberline {2.2}Medici\IeC {\'o}n}{3}
\contentsline {section}{\numberline {2.3}Interfaz gr\IeC {\'a}fica}{4}
\contentsline {section}{\numberline {2.4}Integraci\IeC {\'o}n}{4}
\contentsline {section}{\numberline {2.5}Costo de una soluci\IeC {\'o}n alternativa}{4}
\contentsline {section}{\numberline {2.6}Descripci\IeC {\'o}n de la soluci\IeC {\'o}n propuesta}{5}
\contentsline {section}{\numberline {2.7}Componentes}{5}
\contentsline {chapter}{\numberline {3}Objetivo}{8}
\contentsline {section}{\numberline {3.1}Objetivos espec\IeC {\'\i }ficos}{8}
\contentsline {subsection}{\numberline {3.1.1}Capacidades m\IeC {\'\i }nimas}{8}
\contentsline {subsection}{\numberline {3.1.2}Necesidades espec\IeC {\'\i }ficas}{8}
\contentsline {chapter}{\numberline {4}Disea\IeC {\~n}o preliminar}{10}
\contentsline {section}{\numberline {4.1}Dise\IeC {\~n}o del hardware}{10}
\contentsline {section}{\numberline {4.2}Dise\IeC {\~n}o del software}{10}
\contentsline {section}{\numberline {4.3}Verificaci\IeC {\'o}n de la calidad y Mediciones}{11}
\contentsline {section}{\numberline {4.4}Documentaci\IeC {\'o}n}{12}
\contentsline {section}{\numberline {4.5}Alcance}{13}
\contentsline {subsection}{\numberline {4.5.1}Requerimientos Funcionales}{13}
\contentsline {subsection}{\numberline {4.5.2}Requerimientos opcionales}{13}
\contentsline {section}{\numberline {4.6}Elaboraci\IeC {\'o}n de un an\IeC {\'a}lisis primitivo de costos}{14}
\contentsline {section}{\numberline {4.7}Metodolog\IeC {\'\i }a}{15}
\contentsline {chapter}{\numberline {5}An\IeC {\'a}lisis de mercado}{17}
\contentsline {section}{\numberline {5.1}Definici\IeC {\'o}n del producto}{17}
\contentsline {subsection}{\numberline {5.1.1}Definici\IeC {\'o}n de las caracter\IeC {\'\i }sticas centrales}{17}
\contentsline {subsection}{\numberline {5.1.2} Necesidades de los clientes/consumidores que satisface}{18}
\contentsline {section}{\numberline {5.2} Beneficio para los clientes/consumidores}{18}
\contentsline {section}{\numberline {5.3}Definici\IeC {\'o}n del producto ampliado}{19}
\contentsline {section}{\numberline {5.4}An\IeC {\'a}lisis competitivo}{19}
\contentsline {section}{\numberline {5.5}Competidores}{23}
\contentsline {section}{\numberline {5.6}Estrategia competitiva y plan de acci\IeC {\'o}n}{27}
\contentsline {section}{\numberline {5.7}Plan de negocios}{33}
\contentsline {chapter}{\numberline {6}Dise\IeC {\~n}o de una Herramienta Adaptativa de Desarrollo}{39}
\contentsline {section}{\numberline {6.1}Definici\IeC {\'o}n de las metodolog\IeC {\'\i }as, tecnolog\IeC {\'\i }as y hardware utilizados}{40}
\contentsline {subsection}{\numberline {6.1.1}Elecci\IeC {\'o}n de los componentes}{40}
\contentsline {subsection}{\numberline {6.1.2}El Raspberry Pi}{40}
\contentsline {subsection}{\numberline {6.1.3}El Arduino como interfaz con los sensores}{40}
\contentsline {subsection}{\numberline {6.1.4}Webservice e Interf\IeC {\'a}z gr\IeC {\'a}fica}{40}
\contentsline {subsection}{\numberline {6.1.5}Diagrama de la herramienta}{40}
\contentsline {subsection}{\numberline {6.1.6}Metodolog\IeC {\'\i }a TDD (desarrollo dirigido por tests)}{40}
\contentsline {section}{\numberline {6.2}Desarrollo del sistema de control y mediciones}{40}
\contentsline {subsection}{\numberline {6.2.1}Elecci\IeC {\'o}n del numero y tipo de puertos}{40}
\contentsline {subsection}{\numberline {6.2.2}Esquema de los puertos}{40}
\contentsline {subsubsection}{\numberline {6.2.2.1}Puerto de entrada digital}{40}
\contentsline {subsubsection}{\numberline {6.2.2.2}Puerto de entrada anal\IeC {\'o}gico}{40}
\contentsline {subsubsection}{\numberline {6.2.2.3}Puerto de salida digital}{40}
\contentsline {subsubsection}{\numberline {6.2.2.4}Puerto de salida digital: llave de alta potencia}{40}
\contentsline {subsection}{\numberline {6.2.3}Diagrama de funcionamiento del circuito de control}{40}
\contentsline {section}{\numberline {6.3}Desarrollo del WebService}{40}
\contentsline {subsection}{\numberline {6.3.1}Tecnolog\IeC {\'\i }as usadas y herramientas de desarrollo}{40}
\contentsline {subsection}{\numberline {6.3.2}Diagrama de funcionamiento del web service}{40}
\contentsline {section}{\numberline {6.4}Desarrollo de la interf\IeC {\'a}z gr\IeC {\'a}fica}{40}
\contentsline {subsection}{\numberline {6.4.1}Tecnolog\IeC {\'\i }as usadas y herramientas visuales}{40}
\contentsline {subsubsection}{\numberline {6.4.1.1}HTML5}{40}
\contentsline {subsubsection}{\numberline {6.4.1.2}JavaScript y AJAX}{40}
\contentsline {subsubsection}{\numberline {6.4.1.3}CSS}{40}
\contentsline {subsection}{\numberline {6.4.2}Definici\IeC {\'o}n de una historia de usuario patr\IeC {\'o}n}{40}
\contentsline {subsubsection}{\numberline {6.4.2.1}Diagrama de flujo de la interf\IeC {\'a}z con el usuario}{40}
\contentsline {subsubsection}{\numberline {6.4.2.2}Simplificaci\IeC {\'o}n de la interf\IeC {\'a}z gr\IeC {\'a}fica}{40}
\contentsline {subsection}{\numberline {6.4.3}Diagrama de funcionamiento de la interf\IeC {\'a}z fr\IeC {\'a}fica}{40}
\contentsline {section}{\numberline {6.5}Integraci\IeC {\'o}n de todos los componentes}{40}
\contentsline {subsection}{\numberline {6.5.1}Comunicaci\IeC {\'o}n entre el Raspberry Pi y el Arduino}{40}
\contentsline {subsubsection}{\numberline {6.5.1.1}Desarrollando una interf\IeC {\'a}z para el puerto serial}{40}
\contentsline {subsubsection}{\numberline {6.5.1.2}Velociadades de transferencia de datos}{40}
\contentsline {subsection}{\numberline {6.5.2}Alimentaci\IeC {\'o}n de los componenetes}{40}
\contentsline {subsection}{\numberline {6.5.3}Armado del ADT}{40}
\contentsline {subsubsection}{\numberline {6.5.3.1}Definici\IeC {\'o}n de las dimensi\IeC {\'o}nes y dise\IeC {\~n}o de un contenedor}{40}
\contentsline {subsubsection}{\numberline {6.5.3.2}Puertos del ADT}{40}
\contentsline {chapter}{\numberline {7}An\IeC {\'a}lisis de resultados, posibles mejoras y trabajo futuro}{41}
\contentsline {section}{\numberline {7.1}Resultados y verificaci\IeC {\'o}n de calidad}{41}
\contentsline {section}{\numberline {7.2}Posibles mejoras}{41}
\contentsline {subsection}{\numberline {7.2.1}Detecci\IeC {\'o}n autom\IeC {\'a}tica de sensores}{41}
\contentsline {subsection}{\numberline {7.2.2}Incluci\IeC {\'o}n de sensores remotos (WiFi, Bluetooth, RF)}{41}
\contentsline {subsection}{\numberline {7.2.3}Incluci\IeC {\'o}n de funcionalidad de Sniffer}{41}
\contentsline {section}{\numberline {7.3}Trabajos futuros}{41}
\contentsline {chapter}{\numberline {8}Conclusi\IeC {\'o}n}{42}
\contentsline {chapter}{\numberline {A}Anexo: Bancos de prueba}{43}
\contentsline {section}{\numberline {A.1}Interf\IeC {\'a}z gr\IeC {\'a}fica}{43}
\contentsline {section}{\numberline {A.2}M\IeC {\'o}dulo de control y sensado}{43}
\contentsline {section}{\numberline {A.3}WebService}{43}
\contentsline {chapter}{\numberline {B}Anexo: Documentaci\IeC {\'o}n del usuario}{44}
\contentsline {section}{\numberline {B.1}Manual de usuario}{44}
\contentsline {section}{\numberline {B.2}Ejemplos pr\IeC {\'a}ctico: Configuraci\IeC {\'o}n de un sensor y un actuador}{44}
\contentsline {section}{\numberline {B.3}Documentaci\IeC {\'o}n presentada en el Web Service}{44}
\contentsline {chapter}{\numberline {C}Anexo: Documentaci\IeC {\'o}n t\IeC {\'e}cnica}{45}
\contentsline {section}{\numberline {C.1}Manual t\IeC {\'e}cnico}{45}
\contentsline {section}{\numberline {C.2}Ejemplo pr\IeC {\'a}ctico: Como agregar un modelo de sensor}{45}
\contentsline {chapter}{\numberline {D}Anexo: Incluci\IeC {\'o}n de funcionalidad de sniffer (Preguntar)}{46}
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Bibliograf\'{\i }a}{47}
