import serial
import datetime

STATUS = "PASS"

start = datetime.datetime.now()
print "Starting Test (%s)" % str(start)
print " -"*40

try:

    HOST = serial.Serial('COM3', 9600, timeout = 1)
    CLIENT = serial.Serial('COM7', 9600, timeout = 1)

    print "Verifying sniffer can correctly gather a dataframe"

    DATAFRAME = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    print "Prepare HOST for output"
    HOST.flushOutput()
    HOST.write(DATAFRAME)
    CLIENT.flushInput()
    ret_frame = CLIENT.read()

    print "Check frame received is equal to frame sent... ",
    assert(ret_frame == DATAFRAME)
    print "DONE!"

    raw_input("Place sniffed file in the test folder and press Enter to continue...")
    with open("sniffed_data.txt", "rb") as fp:
        sniffed_data = fp.read_all()
    # Encoding data
    sniffed_data = sniffed_data.encode("UTF-8")

    print "Check frame sent is equal to frame sniffed... ",
    assert(sniffed_data == DATAFRAME)
    print "DONE!"

except:
    STATUS = "FAIL"
end = datetime.datetime.now()
print " -"*40
print "Finished Test (%s)\n" % str(end)
print "="*80
print "STATUS: ", STATUS
print "DURATION: ", end - start
print "="*80
