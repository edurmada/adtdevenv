\contentsline {chapter}{\hbox to\@tempdima {\hfil }Resumen}{\es@scroman {vii}}
\select@language {spanish}
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Lista de figuras}{\es@scroman {ix}}
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Lista de tablas}{\es@scroman {xi}}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}
\contentsline {chapter}{\numberline {2}Evaluaci\IeC {\'o}n de las soluciones existentes y la propuesta}{5}
\contentsline {section}{\numberline {2.1}Control}{5}
\contentsline {section}{\numberline {2.2}Medici\IeC {\'o}n}{5}
\contentsline {section}{\numberline {2.3}Interfaz gr\IeC {\'a}fica}{6}
\contentsline {section}{\numberline {2.4}Integraci\IeC {\'o}n}{6}
\contentsline {section}{\numberline {2.5}Costo de una soluci\IeC {\'o}n alternativa}{6}
\contentsline {section}{\numberline {2.6}Descripci\IeC {\'o}n de la soluci\IeC {\'o}n propuesta}{7}
\contentsline {section}{\numberline {2.7}Componentes}{7}
\contentsline {chapter}{\numberline {3}Objetivo}{10}
\contentsline {section}{\numberline {3.1}Objetivos espec\IeC {\'\i }ficos}{10}
\contentsline {subsection}{\numberline {3.1.1}Capacidades m\IeC {\'\i }nimas}{10}
\contentsline {subsection}{\numberline {3.1.2}Necesidades espec\IeC {\'\i }ficas}{10}
\contentsline {chapter}{\numberline {4}Dise\IeC {\~n}o preliminar}{12}
\contentsline {section}{\numberline {4.1}Dise\IeC {\~n}o del hardware}{12}
\contentsline {section}{\numberline {4.2}Dise\IeC {\~n}o del software}{12}
\contentsline {section}{\numberline {4.3}Verificaci\IeC {\'o}n de la calidad y Mediciones}{13}
\contentsline {section}{\numberline {4.4}Documentaci\IeC {\'o}n}{14}
\contentsline {section}{\numberline {4.5}Alcance}{15}
\contentsline {subsection}{\numberline {4.5.1}Requerimientos Funcionales}{15}
\contentsline {subsection}{\numberline {4.5.2}Requerimientos opcionales}{15}
\contentsline {section}{\numberline {4.6}Elaboraci\IeC {\'o}n de un an\IeC {\'a}lisis primitivo de costos}{16}
\contentsline {section}{\numberline {4.7}Metodolog\IeC {\'\i }a}{17}
\contentsline {chapter}{\numberline {5}An\IeC {\'a}lisis de mercado}{19}
\contentsline {section}{\numberline {5.1}Definici\IeC {\'o}n del producto}{19}
\contentsline {subsection}{\numberline {5.1.1}Definici\IeC {\'o}n de las caracter\IeC {\'\i }sticas centrales}{19}
\contentsline {subsection}{\numberline {5.1.2} Necesidades de los clientes/consumidores que satisface}{20}
\contentsline {section}{\numberline {5.2} Beneficio para los clientes/consumidores}{20}
\contentsline {section}{\numberline {5.3}Definici\IeC {\'o}n del producto ampliado}{21}
\contentsline {section}{\numberline {5.4}An\IeC {\'a}lisis competitivo}{21}
\contentsline {section}{\numberline {5.5}Competidores}{25}
\contentsline {section}{\numberline {5.6}Estrategia competitiva y plan de acci\IeC {\'o}n}{29}
\contentsline {section}{\numberline {5.7}Plan de negocios}{35}
\contentsline {chapter}{\numberline {6}Dise\IeC {\~n}o de una Herramienta Adaptativa de Desarrollo}{41}
\contentsline {section}{\numberline {6.1}Definici\IeC {\'o}n de las metodolog\IeC {\'\i }as, tecnolog\IeC {\'\i }as y hardware utilizados}{42}
\contentsline {subsection}{\numberline {6.1.1}Elecci\IeC {\'o}n de los componentes}{43}
\contentsline {subsection}{\numberline {6.1.2}El Raspberry Pi}{45}
\contentsline {subsection}{\numberline {6.1.3}El Arduino como interfaz con los sensores}{48}
\contentsline {subsection}{\numberline {6.1.4}Webservice e Interf\IeC {\'a}z gr\IeC {\'a}fica}{50}
\contentsline {subsection}{\numberline {6.1.5}Metodolog\IeC {\'\i }a TDD (desarrollo dirigido por tests)}{51}
\contentsline {section}{\numberline {6.2}Desarrollo del sistema de control y mediciones}{54}
\contentsline {subsection}{\numberline {6.2.1}Elecci\IeC {\'o}n del numero y tipo de puertos}{54}
\contentsline {subsection}{\numberline {6.2.2}Esquema de los puertos}{55}
\contentsline {subsubsection}{\numberline {6.2.2.1}Puerto de entrada y salida digital}{55}
\contentsline {subsubsection}{\numberline {6.2.2.2}Puerto de entrada anal\IeC {\'o}gico}{55}
\contentsline {subsubsection}{\numberline {6.2.2.3}Puerto de salida digital: llave de alta potencia}{56}
\contentsline {subsection}{\numberline {6.2.3}Diagrama de funcionamiento del circuito de control}{56}
\contentsline {section}{\numberline {6.3}Desarrollo del WebService}{57}
\contentsline {subsection}{\numberline {6.3.1}Tecnolog\IeC {\'\i }as usadas y herramientas de desarrollo}{57}
\contentsline {subsection}{\numberline {6.3.2}Diagrama de funcionamiento del web service}{63}
\contentsline {section}{\numberline {6.4}Desarrollo de la interf\IeC {\'a}z gr\IeC {\'a}fica}{63}
\contentsline {subsection}{\numberline {6.4.1}Definici\IeC {\'o}n de historias de usuario}{63}
\contentsline {subsubsection}{\numberline {6.4.1.1}Diagrama de funcionamiento de la interfaz con el usuario}{66}
\contentsline {section}{\numberline {6.5}Integraci\IeC {\'o}n de todos los componentes}{68}
\contentsline {subsection}{\numberline {6.5.1}Comunicaci\IeC {\'o}n entre el Raspberry Pi y el Arduino}{68}
\contentsline {subsubsection}{\numberline {6.5.1.1}Desarrollando una interf\IeC {\'a}z para el puerto serial}{68}
\contentsline {subsubsection}{\numberline {6.5.1.2}Protocolo de comunicaci\IeC {\'o}n}{71}
\contentsline {subsection}{\numberline {6.5.2}Alimentaci\IeC {\'o}n de los componenetes}{72}
\contentsline {subsection}{\numberline {6.5.3}Armado del ADT}{73}
\contentsline {subsubsection}{\numberline {6.5.3.1}Definici\IeC {\'o}n de las dimensi\IeC {\'o}nes y dise\IeC {\~n}o de un contenedor}{73}
\contentsline {subsubsection}{\numberline {6.5.3.2}Puertos del ADT}{73}
\contentsline {chapter}{\numberline {7}An\IeC {\'a}lisis de resultados, posibles mejoras y trabajo futuro}{74}
\contentsline {section}{\numberline {7.1}Resultados y verificaci\IeC {\'o}n de calidad}{74}
\contentsline {section}{\numberline {7.2}Posibles mejoras}{74}
\contentsline {subsection}{\numberline {7.2.1}Detecci\IeC {\'o}n autom\IeC {\'a}tica de sensores}{74}
\contentsline {subsection}{\numberline {7.2.2}Inclusi\IeC {\'o}n de sensores remotos (WiFi, Bluetooth, RF)}{74}
\contentsline {subsection}{\numberline {7.2.3}Inclusi\IeC {\'o}n de funcionalidad de Sniffer}{74}
\contentsline {section}{\numberline {7.3}Trabajos futuros}{74}
\contentsline {chapter}{\numberline {8}Conclusi\IeC {\'o}n}{75}
\contentsline {chapter}{\numberline {A}Anexo: Bancos de prueba}{76}
\contentsline {section}{\numberline {A.1}Interf\IeC {\'a}z gr\IeC {\'a}fica}{76}
\contentsline {section}{\numberline {A.2}M\IeC {\'o}dulo de control y sensado}{76}
\contentsline {section}{\numberline {A.3}WebService}{76}
\contentsline {chapter}{\numberline {B}Anexo: Documentaci\IeC {\'o}n del usuario}{77}
\contentsline {section}{\numberline {B.1}Manual de usuario}{77}
\contentsline {section}{\numberline {B.2}Ejemplos pr\IeC {\'a}ctico: Configuraci\IeC {\'o}n de un sensor y un actuador}{77}
\contentsline {section}{\numberline {B.3}Documentaci\IeC {\'o}n presentada en el Web Service}{77}
\contentsline {chapter}{\numberline {C}Anexo: Documentaci\IeC {\'o}n t\IeC {\'e}cnica}{78}
\contentsline {section}{\numberline {C.1}Manual t\IeC {\'e}cnico}{78}
\contentsline {section}{\numberline {C.2}Ejemplo pr\IeC {\'a}ctico: Como agregar un modelo de sensor}{78}
\contentsline {chapter}{\numberline {D}Anexo: Incluci\IeC {\'o}n de funcionalidad de sniffer}{79}
\contentsline {chapter}{\numberline {E}Anexo: C\IeC {\'o}digo}{80}
\contentsline {chapter}{\hbox to\@tempdima {\hfil }Bibliograf\'{\i }a}{81}
